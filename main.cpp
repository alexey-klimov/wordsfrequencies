//
//  main.cpp
//
//  Created by Alexey Klimov on 29.07.16.
//  Copyright © 2016 Alexey Klimov. All rights reserved.
//
// ------------------------------------------------------
// Usage: <executable> input.txt > output.txt
// ------------------------------------------------------


#include <map>
#include <unordered_map>
#include <iostream>
#include <string>
#include <codecvt>

#include <fstream>
#include <sstream>
#include <cassert>

#include <vector>
#include <future>

// ------------------------------------------------------
//  Data types
// ------------------------------------------------------

using ucs_t = wchar_t;
using ucs_istream_t = std::basic_istream<ucs_t>;
using ucs_string_t = std::basic_string<ucs_t>;
using ucs_string_it_t = std::basic_string<ucs_t>::const_iterator;
using word_frequencies_t = std::unordered_map< ucs_string_t, unsigned long >;

// ------------------------------------------------------
//  Platform independent unicode locale creation
// ------------------------------------------------------
const std::locale& get_unicode_locale()
{
    static const std::string locale_name =
        #if defined(__APPLE__) || defined(__linux__)
            "en_US.UTF-8"
        #elif defined(_WIN32) || defined(_WIN64)
            "en_US"
        #else
            #error Unsupported platform
        #endif
    ;
    static const std::locale unicode_locale(locale_name);
    return unicode_locale;
}

// ------------------------------------------------------
//  Converting utf8 text to ucs format
// ------------------------------------------------------
ucs_string_t convert_utf8_to_usc(std::istream& _stream)
{
    std::basic_stringstream< char > buffer;
    buffer << _stream.rdbuf();

    std::wstring_convert< std::codecvt_utf8<ucs_t>, ucs_t > converter;
    ucs_string_t ucs_text = converter.from_bytes(buffer.str());
    //ucs_text.shrink_to_fit(); - causes additional time penalty
    return ucs_text;
}

// ------------------------------------------------------
//  Splitting text by words
// ------------------------------------------------------
ucs_string_t read_usc_word( ucs_string_it_t& _begin, ucs_string_it_t _end )
{
    ucs_string_t word;
    ucs_t s;
    for (;_begin != _end; ++_begin)
    {
        s = *_begin;
        if ( !std::isalpha(s, get_unicode_locale()) )
            break;
        
        word.push_back( std::tolower(s, get_unicode_locale()) );
    }
    return word;
}

void get_words_frequencies(
    word_frequencies_t& _words_frequencies,
    ucs_string_it_t _begin,
    ucs_string_it_t _end
)
{
    assert(_begin <= _end);
    for (;;)
    {
        ucs_string_t word = read_usc_word(_begin, _end);
        if ( word.size() )
            _words_frequencies[word] += 1;

        if (_begin == _end)
            return;
        else
            ++_begin;
    }
}

// ------------------------------------------------------
//  Find points for correct splitting text into thread's units of work
// ------------------------------------------------------
ucs_string_it_t find_nearest_split_point(ucs_string_it_t _begin, ucs_string_it_t _end)
{
    for (;_begin != _end; ++_begin)
    {
        if (!std::isalpha(*_begin, get_unicode_locale()))
            break;
    }
    return _begin;
}

// ------------------------------------------------------
//  Concurrent version of algorithm
// ------------------------------------------------------
word_frequencies_t get_words_frequencies_concurrently( std::istream& _stream )
{
    word_frequencies_t word_frequencies;
    ucs_string_t ucs_text = convert_utf8_to_usc(_stream);
    size_t text_size = ucs_text.size();
    
    size_t n_threads = std::thread::hardware_concurrency();
    if (!n_threads)
        n_threads = 1;
    
    if (text_size <= n_threads)
    {
        get_words_frequencies(word_frequencies, ucs_text.begin(), ucs_text.end());
        return word_frequencies;
    }

    // Storing results per thread.
    size_t n_other_threads = n_threads-1;
    std::vector< std::future<int> > futures(n_other_threads);
    std::vector<word_frequencies_t> map_per_thread(n_other_threads);
    
    // Run concurrently
    ucs_string_it_t begin = ucs_text.begin();
    ucs_string_it_t end = begin + text_size / n_threads;
    for (int i = 0; i < n_other_threads; ++i)
    {
        end = find_nearest_split_point(end, ucs_text.end());
        auto f = std::async(std::launch::async, [ &map_per_thread, begin, end, i]{
            get_words_frequencies(map_per_thread[i], begin, end);
            return i;
        });
        futures[i] = std::move(f);

        begin = end;
        end = ((begin + text_size / n_threads) > ucs_text.end()) ? ucs_text.end() : (begin + text_size / n_threads);
    }
    get_words_frequencies(word_frequencies, begin, end);

    // Merge results
    for (auto &i: futures)
    {
        word_frequencies_t map = map_per_thread[ i.get() ];
        for (auto& pair: map)
            word_frequencies[pair.first] += pair.second;
    }
    
    return word_frequencies;
}

// ------------------------------------------------------
//  Serial version of algorithm
// ------------------------------------------------------
word_frequencies_t get_words_frequencies_serially( std::istream& _stream )
{
    ucs_string_t ucs_text = convert_utf8_to_usc(_stream);

    word_frequencies_t result;
    get_words_frequencies(result, ucs_text.begin(), ucs_text.end());
    return result;
}

// ------------------------------------------------------
//  Printing result
// ------------------------------------------------------
void print( word_frequencies_t const & _frequencies, std::ostream& _utf_stream )
{
    unsigned long total = 0;
    
    std::map< ucs_string_t, unsigned long > sorted_map;
    for ( auto & pair: _frequencies )
        sorted_map[pair.first] = pair.second;
    
    std::wstring_convert< std::codecvt_utf8<ucs_t>, ucs_t > converter;
    for ( auto & pair: sorted_map )
    {
        std::string utf8_string = converter.to_bytes(pair.first);
        _utf_stream << utf8_string << " " << pair.second << std::endl;
        total += pair.second;
    }
    
    std::cout << "Total words count: " << total << std::endl;
}

// ------------------------------------------------------
// Entry point & time measurement
// ------------------------------------------------------
void test_words_frequencies(
    std::function<word_frequencies_t(std::istream&)> _get_words_frequencies
);

template<typename T>
std::chrono::milliseconds measure_execution_time(T _t)
{
    std::chrono::steady_clock clock;
    auto tp_begin = clock.now();
    _t();
    auto tp_end = clock.now();
    auto execution_time = tp_end.time_since_epoch() - tp_begin.time_since_epoch();
    return std::chrono::duration_cast<std::chrono::milliseconds>( execution_time );
}

int main(int argc, const char * argv[]) {
//    test_words_frequencies( get_words_frequencies_serially );
//    test_words_frequencies( get_words_frequencies_serially );
//    return 0;
    
    if (argc != 2)
    {
        std::cout << "Input file should be provided as argument to the program.\n";
        return 1;
    }
    
    // Convert
    std::ifstream file{ argv[1], std::ios_base::in };
    if (file.fail())
        return 1;
    
    word_frequencies_t words_frequencies;
    auto frequencies_calculation_time = measure_execution_time([&]{
        words_frequencies = get_words_frequencies_serially(file);
    });
    
    auto printing_time = measure_execution_time([&]{
        print(words_frequencies, std::cout);
    });
    
    std::cout << "Calculated frequencies in " << frequencies_calculation_time.count() << "ms" << std::endl;
    std::cout << "Printed in " << printing_time.count() << "ms" << std::endl;
    
    return 0;
}

// ------------------------------------------------------
// Testing
// ------------------------------------------------------
ucs_string_t ucs( std::string const & _utf_string)
{
    std::wstring_convert< std::codecvt_utf8<ucs_t>, ucs_t > converter;
    return converter.from_bytes(_utf_string);
}


template <typename T1, typename T2>
bool require_equal_impl(char const * _str, T1 _l, T2 _r)
{
    if (_l != _r)
    {
        std::cerr << _l << " != " << _r << "\n";
    }
    return (_l == _r);
}

#define EQUAL(_l, _r) assert( require_equal_impl(#_l " == " #_r ,_l, _r));

void test_words_frequencies(
    std::function<word_frequencies_t(std::istream&)> _get_words_frequencies
)
{
    {
        std::stringstream text{};
        auto freq = _get_words_frequencies(text);
        EQUAL( freq.size(), 0 );
    }
    
    {
        std::stringstream text{ u8"Г г" };
        auto freq = _get_words_frequencies(text);
        EQUAL( freq.size(), 1 );
        EQUAL( freq[ ucs(u8"г") ], 2 );
    }
    
    {
        std::stringstream text{ u8"Г п !!" };
        auto freq = _get_words_frequencies(text);
        EQUAL( freq.size(), 2 );
        EQUAL( freq[ ucs(u8"г") ], 1 );
        EQUAL( freq[ ucs(u8"п") ], 1 );
    }

    {
        std::stringstream text{ u8"Сплошное" };
        auto freq = _get_words_frequencies(text);
        EQUAL( freq.size(), 1 );
        EQUAL( freq[ ucs(u8"сплошное") ], 1 );
    }
    
    {
        std::stringstream text{ u8"Различные, знаки.препинания!(скобочки)&вопрос?\"кавычки\"" };
        auto freq = _get_words_frequencies(text);
        EQUAL( freq.size(), 6 );
        EQUAL( freq[ ucs(u8"различные") ], 1 );
        EQUAL( freq[ ucs(u8"знаки") ], 1 );
        EQUAL( freq[ ucs(u8"препинания") ], 1 );
        EQUAL( freq[ ucs(u8"скобочки") ], 1 );
        EQUAL( freq[ ucs(u8"вопрос") ], 1 );
        EQUAL( freq[ ucs(u8"кавычки") ], 1 );
    }
    
    {
        std::stringstream text{ u8"Some` english words in test #6" };
        auto freq = _get_words_frequencies(text);
        EQUAL( freq.size(), 5 );
        EQUAL( freq[ ucs(u8"some") ], 1 );
        EQUAL( freq[ ucs(u8"english") ], 1 );
        EQUAL( freq[ ucs(u8"words") ], 1 );
        EQUAL( freq[ ucs(u8"in") ], 1 );
        EQUAL( freq[ ucs(u8"test") ], 1 );
    }
    
    std::cout << "*** SUCCESS ***\n";
}

/*---------------------------------------------------------------*/